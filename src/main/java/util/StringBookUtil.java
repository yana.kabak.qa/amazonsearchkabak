package util;

public class StringBookUtil {

    private final static String FIND = "by";

    public String convertAuthor(String author){
        author = author.substring(author.indexOf(FIND) + FIND.length());
        author = author.replaceAll(",","");
        author = author.replace("(Author)", "");
        author = author.replace("\n", "and ");
        author = author.substring(author.indexOf(" ")+1);
        return author;
    }
}
