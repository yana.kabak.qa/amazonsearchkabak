package action;

import org.openqa.selenium.WebDriver;
import page.BookPage;
import object.Book;

public class BookAction {
    BookPage bookPage;

    public BookAction(WebDriver webDriver){
        bookPage = new BookPage(webDriver);
    }

    public Book findBook(){
        Book book = new Book();
        book.setName(bookPage.setNameBook());
        book.setAuthor(bookPage.setAuthor());
        book.setBestseller(false);
        book.setPricePaper(bookPage.setPricePaper());
        return book;
    }
}
