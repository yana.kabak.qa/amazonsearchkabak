package action;

import object.Book;
import org.openqa.selenium.WebDriver;
import page.SearchFormPage;
import util.StringFormUtil;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class FormAction {
    WebElement selectElement;
    List<Book> books;
    SearchFormPage searchFormPage;
    StringFormUtil stringFormUtil;
    String wordSearch = "Java";
    String res;
    String regPrice;
    String id;

    public FormAction(WebDriver webDriver){
        searchFormPage = new SearchFormPage(webDriver);
        stringFormUtil = new StringFormUtil();
    }
    public void selectCategoryBooks (){
        selectElement = searchFormPage.setSelectForName();
        Select select = new Select(selectElement);
        select.selectByVisibleText("Books");
    }

    public void setName (){  searchFormPage.setSendKey().sendKeys(wordSearch); }
    public void submit() { searchFormPage.setButtonName().submit();}
    public void setFilterRussian(){ searchFormPage.setNameForFilter().click();}

    public List<Book> findBooks(){
        books = new ArrayList<>();
        for (int i=0; i<searchFormPage.setArrayOfBookName().size(); i++){
            Book book = new Book();
            book.setName(searchFormPage.setArrayOfBookName().get(i).getText());
            res = searchFormPage.setArrayOfAuthorName().get(i).getText();
            book.setAuthor(stringFormUtil.convertAuthor(res));
            regPrice = searchFormPage.setArrayOfPaperPrice().get(i).getText();
            book.setPricePaper(stringFormUtil.convertPrice(regPrice));
            id = stringFormUtil.convertStringID(searchFormPage.setIdOfBook());
            if(id.equals(searchFormPage.setArrayOfBooksId().get(i).getText())){
                book.setBestseller(true);
            }
            books.add(book);
            System.out.println(book);
        }
        return books;
    }

    public List<Book> findRussianBooks(){
        books = new ArrayList<>();
        for (int i=0; i<searchFormPage.setArrayOfBookName().size(); i++){
            Book book = new Book();
            book.setName(searchFormPage.setArrayOfBookName().get(i).getText());
            res = searchFormPage.setArrayOfAuthorName().get(i).getText();
            book.setAuthor(stringFormUtil.convertAuthor(res));
            regPrice = searchFormPage.setArrayOfPaperPrice().get(i).getText();
            book.setPricePaper(stringFormUtil.convertPrice(regPrice));
            book.setBestseller(false);
            books.add(book);
        }
        return books;
    }

    public boolean equalsBook(List<Book> books, Book book){
        for (Book value : books) {
            if (value.equals(book)) {
                return true;
            }
        }
        return false;
    }
}
