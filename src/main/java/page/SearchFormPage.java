package page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SearchFormPage {
    WebDriver driver;
    By tagName = By.tagName("select");
    By name = By.name("field-keywords");
    By nameButton = By.xpath("//*[@id='nav-search-submit-button']");
    By booksName = By.xpath("//*[@class='a-size-medium a-color-base a-text-normal']");
    By booksAuthor = By.xpath("//*[@class='a-row a-size-base a-color-secondary']//div[@class='a-row']");
    By bestSeller = By.xpath("//*[@class='a-badge-label']");
    By bookPrice = By.xpath("//*[@class='a-section a-spacing-none a-spacing-top-small']");
    By bookID = By.xpath("//*[@class='s-result-item s-asin sg-col-0-of-12 sg-col-16-of-20 sg-col sg-col-12-of-16']");
    By bookFilter = By.xpath("//ul[@class='a-unordered-list a-nostyle a-vertical a-spacing-medium']/li[@id='p_n_feature_nine_browse-bin/3291441011']/span/a");

    public SearchFormPage(WebDriver driver) {
        this.driver = driver;
    }

    @Step("Selected name for set the filter for books language")
    public WebElement setNameForFilter(){ return driver.findElement(bookFilter);}

    @Step("Selected name for set the filter in search engine")
    public WebElement setSelectForName(){
        return driver.findElement(tagName);
    }

    @Step("Send key word for searching")
    public WebElement setSendKey(){
        return driver.findElement(name);
    }

    @Step("Press button for searching")
    public WebElement setButtonName(){
        return driver.findElement(nameButton);
    }

    @Step("Get array of books name")
    public List<WebElement> setArrayOfBookName(){
        return driver.findElements(booksName);
    }

    @Step("Get array of books author")
    public List<WebElement> setArrayOfAuthorName(){
        return driver.findElements(booksAuthor);
    }

    @Step("Get array of books price")
    public List<WebElement> setArrayOfPaperPrice(){
        return driver.findElements(bookPrice);
    }

    @Step("Get ID of best seller book")
    public String setIdOfBook(){
        return driver.findElement(bestSeller).getAttribute("id");
    }

    @Step("Get Array of books id")
    public List<WebElement> setArrayOfBooksId(){
        return driver.findElements(bookID);
    }
}
