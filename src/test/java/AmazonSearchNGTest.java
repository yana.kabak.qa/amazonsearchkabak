import action.BookAction;
import action.FormAction;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import object.Book;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;

public class AmazonSearchNGTest {
    BookAction amazonSearch;
    FormAction formAction;
    WebDriver webDriver;
    List<Book> booksLink;
    Book book;
    private final static String AmazonBookURL="https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_2?dchild=1&keywords=Java&qid=1610356790&s=books&sr=1-2";
    private final static String AmazonManePage="https://www.amazon.com/";
    private final static String AmazonRussianBookPage = "https://www.amazon.com/Java-взрослых-Russian-Alex-Nsky-ebook/dp/B098B8425K/ref=sr_1_1?dchild=1&keywords=java&qid=1625582495&refinements=p_n_feature_nine_browse-bin%3A3291441011&rnid=3291435011&s=books&sr=1-1";

    @BeforeTest
    public void profileSetup() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        webDriver = new ChromeDriver();
        webDriver.manage().window().maximize();
        webDriver.get(AmazonManePage);
        formAction = new FormAction(webDriver);
        amazonSearch = new BookAction(webDriver);
        System.out.println("The profile setup process is completed");
    }

    @Severity(SeverityLevel.CRITICAL)
    @Description("Comparison books from searching and test book")
    @Story("Test comparison books")
    @Test
    public void searchBook(){
        formAction.selectCategoryBooks();
        formAction.setName();
        formAction.submit();
        booksLink = formAction.findBooks();
        webDriver.get(AmazonBookURL);
        book = amazonSearch.findBook();
        Assert.assertTrue(formAction.equalsBook(booksLink,book));
    }

    @Severity(SeverityLevel.CRITICAL)
    @Description("Comparison books from searching and test book")
    @Story("Test comparison russian books")
    @Test
    public void searchRussianBook(){
        formAction.selectCategoryBooks();
        formAction.setName();
        formAction.submit();
        formAction.setFilterRussian();
        booksLink = formAction.findRussianBooks();
        webDriver.get(AmazonRussianBookPage);
        book = amazonSearch.findBook();
        Assert.assertTrue(formAction.equalsBook(booksLink,book));
    }

    @AfterTest
    public void after(){
        webDriver.quit();
    }

}
