package util;

public class StringFormUtil {

    private final static String FIND = "by";
    public String convertStringID(String bestSellerID){
        bestSellerID = bestSellerID.split("-",2)[0];
        return bestSellerID;
    }

    public String convertAuthor(String author){
        author = author.substring(author.indexOf(FIND) + FIND.length());
        author = author.split("\\|", 2)[0];
        return author;
    }

    public String convertPrice(String price){
        price = price.substring(price.indexOf("$")+1);
        price = price.replaceAll("\n", ".");
        price = price.split("\\$",2)[0];
        price = price.replaceFirst( "(.*?) .*", "$1");
        if(price.contains("Paperback")){
            return "Only Kindle";
        }else {
            if(price.endsWith("."))
            {
                price = price.replaceFirst(".$","");
            }
            return "$" + price;
        }
    }
}
