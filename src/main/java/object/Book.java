package object;

import java.util.Objects;

public class Book {
    private String name;
    private String author;
    private String pricePaper;
    private boolean bestseller;

    public Book(String name, String author, String pricePaper, boolean bestseller) {
        this.name = name;
        this.author = author;
        this.bestseller = bestseller;
        this.pricePaper = pricePaper;
    }

    public Book(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isBestseller() {
        return bestseller;
    }

    public String getPricePaper() {
        return pricePaper;
    }

    public void setPricePaper(String pricePaper) {
        this.pricePaper = pricePaper;
    }

    public void setBestseller(boolean bestseller) {
        this.bestseller = bestseller;
    }

    @Override
    public String toString() {
        return "object.Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", bestseller=" + bestseller + '\'' +
                ", price Paper=" + pricePaper +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(name, book.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
