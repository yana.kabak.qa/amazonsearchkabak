package page;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import util.StringBookUtil;

public class BookPage {
    By bookName = By.xpath("//*[@class='a-size-extra-large']");
    By bookAuthor = By.xpath("//*[@class='a-section a-spacing-micro bylineHidden feature']");
    By bookPrice = By.xpath("//*[@class='a-size-base a-color-price a-color-price']");
    WebDriver driver;
    StringBookUtil stringBookUtil;

    public BookPage(WebDriver driver) {
        this.driver = driver;
        stringBookUtil = new StringBookUtil();
    }

    @Step("Get book name on Amazon")
    public String setNameBook(){
        return driver.findElement(bookName).getText();
    }

    @Step("Get author of book name from Amazon")
    public String setAuthor(){
        return stringBookUtil.convertAuthor(driver.findElement(bookAuthor).getText());
    }

    @Step("Get price of paper version of book")
    public String setPricePaper(){
        return driver.findElement(bookPrice).getAttribute("data-asin");
    }
}
